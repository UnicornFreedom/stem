FROM rust

WORKDIR /stem
COPY ./ ./
RUN cargo build --release
RUN mkdir -p /build-out
CMD target/release/stem