## STEM
Lightweight internet bridge.  
Created with OpenComputers in mind and goes with Lua API library.

### Usage
From the client-side you will need to `require` the library,
open the connection and listen for the event `stem_message`.

```lua
local event = require('event')

-- import STEM client library
local stem = require('stem')

-- open a connection to a STEM server
local server = stem.connect('stem.fomalhaut.me')

-- add a listener for messages from the channel with ID 'my-channel-id'
server:subscribe('my-channel-id')

-- listen for events in a loop...
while true do
  local name, channel_id, message = event.pull('stem_message')
  if name ~= nil then
    print(channel_id, message)
  end
end

-- ...or register a listener
event.listen('stem_message', function(_, _, channel_id, message)
  print(channel_id, message)
end)

-- you can also send message to the channel
server:send('my-channel-id', 'hello there')

-- unsubscribe from channel events
server:unsubscribe('my-channel-id')

-- and completely close the connection to the STEM server
server:disconnect()
```

You can use several channels and even servers simultaneously.

### Server setup
Compile the server code using the stable Rust branch, and then run the build.  
Done.

Server can be customized using the `stem.toml` file in the working directory.  
For example, the default configuration would look like this:

```toml
[tcp]
host = '127.0.0.1'
port = 5733

[web]
host = '127.0.0.1'
port = 5780

[general]
ping_interval = 60
```

Use `RUST_LOG` environment variable to manage app logs. For example like this:
```
$ RUST_LOG=stem ./stem
```
to enable all logging.
