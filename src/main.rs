#[macro_use]
extern crate actix;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate tera;

use actix::prelude::*;
use actix_web::{
    server::HttpServer, fs::StaticFiles,
    App, AsyncResponder, Error, FutureResponse, HttpResponse, Query, State,
    middleware, http, error, ws
};
use futures::*;
use log::info;
use tokio::net::tcp::TcpListener;

use std::collections::HashMap;
use std::net::SocketAddr;
use std::str::FromStr;

mod actor;
mod config;
mod util;

use config::Config;
use actor::server::{Server, NewTCPConnection, StatsRequest};
use actor::webclient::WebClient;
use util::webappstate::WebAppState;

fn index(state: State<WebAppState>) -> FutureResponse<HttpResponse> {
    state.server.send(StatsRequest)
        .map_err(|_| error::ErrorInternalServerError("Stats error"))
        .map(move |stats| {
            let mut ctx = tera::Context::new();
            ctx.insert("channels", &stats.0);
            ctx.insert("sessions", &stats.1);
            state.template
                .render("index.html", &ctx)
                .map(|s| HttpResponse::Ok().content_type("text/html").body(s))
                .map_err(|_| error::ErrorInternalServerError("Template error"))
        })
        .flatten()
        .responder()
}

fn channel((state, _): (State<WebAppState>, Query<HashMap<String, String>>)) -> Result<HttpResponse, Error> {
    let s = {
        let ctx = tera::Context::new();
        state.template
            .render("channel.html", &ctx)
            .map_err(|_| error::ErrorInternalServerError("Template error"))?
    };
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

fn main() {
    System::run(|| {
        env_logger::init();
        let config = Config::load("stem.toml");

        // create TCP listener
        let tcp_path = &(config.tcp.host + ":" + &config.tcp.port.to_string());
        let addr = SocketAddr::from_str(tcp_path).unwrap();
        let listener = TcpListener::bind(&addr).unwrap();

        // start parent actor
        let ping_interval = config.general.ping_interval;
        let server = Server::create(move |ctx| {
            ctx.add_message_stream(listener.incoming().map_err(|_| ()).map(|st| {
                let addr = st.peer_addr().unwrap();
                NewTCPConnection(st, addr)
            }));
            Server::new(ping_interval)
        });

        // create Web server
        let web_path = &(config.web.host + ":" + &config.web.port.to_string());
        HttpServer::new(move || {
            // check that static files are in place
            let static_files = StaticFiles::new("static/");
            let static_files = match static_files {
                Ok(files) => files,
                Err(_) => panic!("Cannot open the './static/' directory. Make sure it is in the same folder as STEM binary.")
            };

            let tera = compile_templates!("templates/**/*");
            let state = WebAppState { template: tera, server: server.clone() };
            let server_clone = server.clone();
            App::with_state(state)
                .middleware(middleware::Logger::default())
                .handler("/static", static_files)
                .resource("/ws", move |r| r.f(move |req| ws::start(req, WebClient::new(req.peer_addr().unwrap(), server_clone.clone()))))
                .resource("/channel", |r| r.method(http::Method::GET).with(channel))
                .default_resource(|r| r.method(http::Method::GET).with(index))
        })
            .bind(web_path)
            .unwrap()
            .start();

        info!("[INFO] TCP server started on {}...", tcp_path);
        info!("[INFO] Web-server started on {}...", web_path);
    });
}
