use ::actix::prelude::*;
use crate::util::codec::Package;

#[derive(Message)]
pub struct ClientPackage {
    pub address: Recipient<Package>,
    pub package: Package
}

#[derive(Message)]
pub enum ClientStatus {
    Started(Recipient<Package>),
    Stopped(Recipient<Package>),
}
